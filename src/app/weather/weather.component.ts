import { Component} from '@angular/core';
import {WeatherDataService} from "../weather-data.service";
import {Root} from "./weather-interfaces";
import {Observable, Subscription} from "rxjs";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent {
  private rootSubscription: Subscription;
  public root$: Observable<Root>;
  constructor(private weatherData: WeatherDataService) {
    this.root$ = null;
    this.rootSubscription =
    this.weatherData.getWeather().subscribe(
      data => {
        this.root$ = data;
      }
    );
  }
}
