import { Component, OnInit } from '@angular/core';
import {BookDataService} from "../book-data.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent {
  public books: Object;
  constructor(private bookData: BookDataService) {
    this.bookData.getBooks().subscribe(
      data => this.books = data
    );
  }
}
