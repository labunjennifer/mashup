import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {Root} from "./weather/weather-interfaces";
import {Observable} from "rxjs";

@Injectable({
  providedIn: "root",
})
export class WeatherDataService {

  constructor(private http: HttpClient) {}

  getWeather() {
    return this.http.get<Observable<Root>>("https://api.openweathermap.org/data/2.5/weather?q=Rom&units=metric&appid=53c1364f284e7103e09c9b3ca80d5c8b");
  }
}
